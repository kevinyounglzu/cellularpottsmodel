/*
 * =====================================================================================
 *
 *       Filename:  test_cell.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  11/25/2014 14:17:04
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include "minunit.h"
#include <dbg.h>
#include <cmp.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

char *test_cell_initialize()
{
    Cell my_cell = Cell_new();
    Cell *p_cell = &my_cell;

    Cell_setup(1000, 100, p_cell);

    mu_assert(my_cell.left == 950, "Wrong left value");
    mu_assert(my_cell.right == 1050, "Wrong right value");

    return NULL;
}

char *test_generate_position()
{
    mu_assert(1000 == generatePosition(40, 60, 0.5, 10), "Wrong postion");
    return NULL;
}

char *test_cell_helper_function()
{
    Cell the_cell;
    the_cell.left = 10;
    the_cell.right = 20;
    Cell *p_cell = &the_cell;

    // test gets
    mu_assert(Cell_get_left(p_cell) == 10, "Wrong left value");
    mu_assert(Cell_get_right(p_cell) == 20, "Wrong right value");
    mu_assert(Cell_get_position(p_cell) == 15, "Wrong position");
    mu_assert(Cell_get_length(p_cell) == 10, "Wrong length");

    // test adjustments
    Cell_left_left(p_cell);
    mu_assert(Cell_get_left(p_cell) == 8, "Wrong left value");
    Cell_left_right(p_cell);
    mu_assert(Cell_get_left(p_cell) == 10, "Wrong left value");
    Cell_right_left(p_cell);
    mu_assert(Cell_get_right(p_cell) == 18, "Wrong right value");
    Cell_right_right(p_cell);
    mu_assert(Cell_get_right(p_cell) == 20, "Wrong right value");

    // test copy
    Cell buffer_cell;
    buffer_cell.left = 70;
    buffer_cell.right = 90;
    Cell *p_buffer = &buffer_cell;
    Cell_copy(p_cell, p_buffer);
    mu_assert(Cell_get_left(p_buffer) == 10, "Wrong buffer left value.");
    mu_assert(Cell_get_right(p_buffer) == 20, "Wrong buffer right value.");

    return NULL;
}

char *test_random_generate()
{
    FILE *fp;
    char *file_name = "./data/randomnumber.txt";
    char random_number[100];

    int i;
//    srand(time(NULL));

    fp = fopen(file_name, "w");

    for(i=0; i<10; i++)
    {
        sprintf(random_number, "%d", rand());
        printf("%s\n", random_number);
//        fprintf(fp, (char *)random_number);
    }
    fclose(fp);

    return NULL;
}

char *test_chemical_1()
{
    int i = 0;
    for(i=0; i<100; i++)
    {
        printf("%d: %f\n",i,chemical1((double)i));
    }
    return NULL;
}

char *test_boltzmann()
{
    int i = 0;
    for(i=0; i<10; i++)
    {
        printf("%d: %f\n", i, boltzmannProbDist(i/100.));
    }
    return NULL;
}

double returnOne(x)
{
    x += 1;
    return 1;
}
char *test_energy_funciton()
{
    int i = 0;
    for(i=0; i<10; i++)
    {
        printf("%d: %f\n",i, energyFunction(1.0, i, returnOne));
    }
    return NULL;
}

double lineDist(double x)
{
    return x;
}

char *test_cellular_potts_judge()
{
    int rc;
    // energy < 0 should return 1
    rc = cellularPottsJudge(-1., 1., lineDist);
    mu_assert(rc == 1, "Wrong return value when energy < 0.");

    // energy > 0 depends
    rc = cellularPottsJudge(2., 1., lineDist);
    mu_assert(rc == 1, "Wrong return value when energy > 0");

    rc = cellularPottsJudge(0.5 , 1., lineDist);
    mu_assert(rc == 0, "Wrong return value when energy > 0");
    return NULL;
}

char *all_tests()
{
    mu_suite_start();

    mu_run_test(test_cell_initialize);
//    mu_run_test(test_random_generate);
    mu_run_test(test_cell_helper_function);
//    mu_run_test(test_energy_funciton);
//    mu_run_test(test_boltzmann);
//    mu_run_test(test_one_trial);
    mu_run_test(test_cellular_potts_judge);
    mu_run_test(test_generate_position);
    return NULL;
}

RUN_TESTS(all_tests);
