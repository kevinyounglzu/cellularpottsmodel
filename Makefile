CFLAGS=-g -O2 -Wall -Wextra -Isrc -DNDEBUG $(OPTFLAGS)
GCC=gcc

SOURCES=$(wildcard src/*.c)
OBJECT=$(patsubst src/%.c,build/%.o,$(SOURCES))

TEST_SRC=$(wildcard tests/*.c)
TESTS=$(patsubst %.c,%,$(TEST_SRC))

MAIN_SRC=$(wildcard bin/*.c)
MAIN=$(patsubst %.c,%,$(MAIN_SRC))

all: tests

main: $(MAIN)

run: $(MAIN)
	./$(MAIN)

$(MAIN): $(MAIN_SRC) $(OBJECT)
	$(GCC) $(CFLAGS) $(MAIN_SRC) $(OBJECT) -o $(MAIN) -lm

$(OBJECT): build $(SOURCES)
	$(GCC) $(CFLAGS) -c $(SOURCES) -o $@

tests: $(TESTS)
	./$(TESTS)

$(TESTS): $(TEST_SRC) $(OBJECT)
	$(GCC) $(CFLAGS) $(TEST_SRC) $(OBJECT) -o $(TESTS)

build:
	@mkdir -p build
	@mkdir -p bin

clean:
	rm -f $(OBJECT)
	rm -f $(MAIN)
	rm -f $(TESTS)
	rm -rf `find . -name "*.dSYM" -print`
