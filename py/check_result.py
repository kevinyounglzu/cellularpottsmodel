# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    file_name = "./dist.txt"
    position = "./py/postion.txt"
    timeseries = "./data/end.txt"
    time_data = np.loadtxt(timeseries)
    #position_data = np.loadtxt(position)
    #dist_data = np.loadtxt(file_name)
    #print dist_data
    #print len(dist_data)
    #plt.hist(dist_data)
    #plt.hist(position_data)
    #time_steps = len(time_data)
    #discretetime = np.arange(time_steps)
    #plt.plot(discretetime, time_data)
    plt.hist(time_data, bins=100)
    plt.show()
