# -*- coding: utf-8 -*-
import math

def chemical(x):
    return (x - 70)**2 / 400.


def energy(x, l):
    lamb = 4
    lt = 5
    jcm = 2
    mu = 0.1

    return 2 * jcm * l + lamb * (l - lt)**2 + mu * l


def boltzmann(energy):
    beta = 15
    return math.exp(- beta * energy)


if __name__ == "__main__":
    #for l in range(10):
        ##print i, chemical(i)
        #print l, energy(1, l)
    for i in range(10):
        print i, boltzmann(i/100.)
