/*
 * =====================================================================================
 *
 *       Filename:  cmp.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  11/25/2014 13:12:38
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <cmp.h>

int generatePosition(int lowerbound, int upperbound, double random_number, int varepsilon)
{
    upperbound = 2 * upperbound * varepsilon;
    lowerbound = 2 * lowerbound * varepsilon;
    double r = (upperbound - lowerbound) * random_number;
    return lowerbound + (int)r;
}

int generateLength(int position, double random_number, int varepsilon)
{
    position += 1;
    random_number += 1.;
    return 5 * varepsilon * 2;
}

Cell Cell_new()
{
    Cell new_cell;
    new_cell.left = 0;
    new_cell.right = 0;

    return new_cell;
}

void Cell_setup(int x, int l, Cell *the_cell)
{
    the_cell->left = x - l / 2;
    the_cell->right = x + l / 2;
}

int oneTimeStep(Cell *my_cell, Cell *buffer_cell,  double random_number1, double random_number2, Probability_dist dist, int varepsilon)
{
    // decide which way to go
    // 1. my_cell.left - 1
    // 2. my_cell.left + 1
    // 3. my_cell.right - 1
    // 4. my_cell.right + 1
    if(random_number1 < 0.25)
    {
        oneTrial(my_cell, buffer_cell, Cell_left_left, random_number2, dist, varepsilon);
    }
    else if(random_number1 < 0.5)
    {
        oneTrial(my_cell, buffer_cell, Cell_left_right, random_number2, dist, varepsilon);
    }
    else if(random_number1 < 0.75)
    {
        oneTrial(my_cell, buffer_cell, Cell_right_left, random_number2, dist, varepsilon);
    }
    else
    {
        oneTrial(my_cell, buffer_cell, Cell_right_right, random_number2, dist, varepsilon);
    }
    return 1;
}

int oneTrial(Cell *my_cell, Cell *buffer_cell, OperationOverCell my_operation, double random_number, Probability_dist dist, int varepsilon)
{
    Cell_copy(my_cell, buffer_cell);
    my_operation(buffer_cell);

    double my_energy = energyFunction(Cell_get_position(my_cell, varepsilon), Cell_get_length(my_cell, varepsilon), chemical1);
    double buffer_energy = energyFunction(Cell_get_position(buffer_cell, varepsilon), Cell_get_length(buffer_cell, varepsilon), chemical1);

    if(cellularPottsJudge(buffer_energy - my_energy, random_number, dist))
    {
        my_operation(my_cell);
    }

    return 1;
}

int cellularPottsJudge(double delta_energy, double random_number, Probability_dist dist)
{
    if(delta_energy <= 0)
    {
        // it's going to happen
        return 1;
    }
    else
    {
        // use dist to see if it's going to happen
        if(random_number < dist(delta_energy))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}

double energyFunction(double x, double l, chemicalFunction chemicalField)
{
    return 2 * JCM * l + LAMBDA * (l - LT) * (l - LT) + MU * chemicalField(x) * l;
}

double chemical1(double x)
{
    return (x - 70) * (x - 70) / 400.;
}

double boltzmannProbDist(double energy)
{
    return exp(- BETA * energy);
}
