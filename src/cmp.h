/*
 * =====================================================================================
 *
 *       Filename:  cmp.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  11/25/2014 14:19:26
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef _CMP_h
#define _CMP_h

#include <time.h>
#include <stdlib.h>
#include <math.h>

#define LAMBDA 4
#define LT 5
#define JCM 2
#define BETA 15
#define MU 0.1

typedef struct Cell {
    int left;  // left cordinate of the cell
    int right; // right cordinate of the cell
} Cell;

typedef void (*OperationOverCell)(Cell *the_cell);

// energy functions
typedef double (*chemicalFunction)(double x);
double energyFunction(double x, double l, chemicalFunction chemicalField);
double chemical1(double x);

// probability functions
typedef int (*Probability_judge)(double energy);
typedef double (*Probability_dist)(double energy);
int cellularPottsJudge(double delta_energy, double random_number, Probability_dist dist);
double boltzmannProbDist(double energy);

int oneTimeStep(Cell *my_cell, Cell *buffer_cell,  double random_number1, double random_number2, Probability_dist dist, int varepsilon);
int oneTrial(Cell *my_cell, Cell *buffer_cell, OperationOverCell my_operation, double random_number, Probability_dist dist, int varepsilon);

// some helper function
Cell Cell_new();  // return a new cell struct
int generatePosition(int lowerbound, int upperbound, double random_number, int varepsilon);
int generateLength(int position, double random_number, int varepsilon);
void Cell_setup(int x, int l, Cell *the_cell);
#define Cell_get_left(A) (A->left)
#define Cell_get_right(A) (A->right)
static inline double Cell_get_position(Cell *the_cell, int varepsilon)
{
    // get the position of the_cell
    return (the_cell->right + the_cell->left) / (double)(4 * varepsilon);
}
static inline double Cell_get_length(Cell *the_cell, int varepsilon)
{
    // get the length of the_cell
    return (the_cell->right - the_cell->left) / (double)(2 * varepsilon);
}
// cell boundry adjustment
static inline void Cell_left_left(Cell *the_cell)
{
    // left - 1
    the_cell->left -= 2;
}
static inline void Cell_left_right(Cell *the_cell)
{
    // left + 1
    the_cell->left += 2;
}
static inline void Cell_right_left(Cell *the_cell)
{
    // right - 1
    the_cell->right -= 2;
}
static inline void Cell_right_right(Cell *the_cell)
{
    // right + 1
    the_cell->right += 2;
}
static inline void Cell_copy(Cell *the_cell, Cell *target_cell)
{
    // copy the_cell to target_cell
    target_cell->left = the_cell->left;
    target_cell->right = the_cell->right;
}

#endif
