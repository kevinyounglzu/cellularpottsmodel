/*
 * =====================================================================================
 *
 *       Filename:  main_program.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  11/25/2014 18:07:19
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <cmp.h>
#include <stdio.h>

double generateRandom(void)
{
    return rand() / (double)RAND_MAX;
}
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ()
{
    int N = 1000;
    int varepsilon = 100;
    int time_steps = varepsilon * varepsilon * 200;
//    char file_name[100];
    FILE *fp;

    int i, j;
    int x, l;
    // seed the random number generator
    srand(time(NULL));
    generateRandom();
    generateRandom();

    // get a simulation cell and a buffer_cell
    Cell o_my_cell = Cell_new();
    Cell *my_cell = &o_my_cell;

    Cell o_buffer_cell = Cell_new();
    Cell *buffer_cell = &o_buffer_cell;


    fp = fopen("./data/end.txt", "w");
    for(j=0; j<N; j++)
    {
        x = generatePosition(40, 60, generateRandom(), varepsilon);
        l = generateLength(x, generateRandom(), varepsilon);
        Cell_setup(x, l, my_cell);

//        sprintf(file_name, "./data/%d.txt", j);
//        fp = fopen(file_name, "w");

        for(i=0; i<time_steps; i++)
        {
    //        printf("%f %f\n", Cell_get_position(my_cell, varepsilon), Cell_get_length(my_cell, varepsilon));
//            fprintf(fp, "%f\n", Cell_get_position(my_cell, varepsilon));
            oneTimeStep(my_cell, buffer_cell, generateRandom(), generateRandom(), boltzmannProbDist, varepsilon);
        }
//        fclose(fp);
        fprintf(fp, "%f\n", Cell_get_position(my_cell, varepsilon));
    }
    fclose(fp);

    return 0;
}
/* ----------  end of function main  ---------- */
